# CT30A3401 - Distributed Systems, Practical assignment

This repository contains the code of the 3 basic tasks given for the course.  
Each task's documentation can be found inside the task folder.

Author: Andrey Aleksandrov
