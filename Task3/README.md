# Task 3: Asynchronous file upload

This service allows the user to upload a `.csv` file through a *very* simple web interface.
The file is then parsed into JSON objects and persisted in a MongoDB database.
Another endpoint can be used to query objects from the database.

## Usage

The service runs inside Docker containers which are controlled using `docker-compose`.
To start the service simply run:
```
docker-compose build
docker-compose up
```

After the containers are up, the web interface can be accessed at `localhost:3000`. (`localhost:3000/top` for queries.)
