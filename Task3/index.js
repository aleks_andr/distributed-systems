const express = require('express');
const csvToJson = require('csvtojson');
const fileUpload = require('express-fileupload');
const MongoClient = require('mongodb').MongoClient;

const MONGO_URL = 'mongodb://mongodb:27017/data';

const app = express();

let promise;

const getDatabase = () => promise || (promise = MongoClient.connect("mongodb://mongodb:27017/csv-data", { useNewUrlParser: true }).then((client) => client.db()));

app.use(fileUpload({
    useTempFiles: true
}));

app.use("/", express.static('public'));

app.get("/", async (req, res) => {
    return res.send("OK");
});

app.post("/upload", async (req, res) => {
    try {
        const jsonArray = await csvToJson().fromFile(req.files.csvfile.tempFilePath);
        const records = (await getDatabase()).collection("records");

        await records.insertMany(jsonArray);

        return res.sendStatus(200);
    } catch (err) {
        return res.sendStatus(500);
    }
});

app.get('/top', async (req, res) => {
    const { field } = req.query;

    try {
        const count = parseInt(req.query.count);
        const records = (await getDatabase()).collection("records");
        const data = await records.find().sort({[field]: -1}).limit(count).toArray();

        return res.send(data);

    } catch (err) {
        console.log(err);
        return res.sendStatus(500);
    }
});

app.listen(3000, () => console.log('Listening on port 3000'));
