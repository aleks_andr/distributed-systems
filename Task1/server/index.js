const PORT = 3030;

const app = require('express')();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

io.on('connection', (socket) => {

    // Handle new connections
    socket.user = socket.handshake.query.username;
    console.log(`User connected with name: ${socket.user}`);

    socket.broadcast.emit('user_connected', {
        user: socket.user
    });

    // Propagate user's message to other clients
    socket.on('message', (message) => {
        console.log(`Message: "${message}" from user: ${socket.user}`);
        io.emit('message', {
            user: socket.user,
            message
        });
    });

    // Inform other clients when user leaves
    socket.on('disconnect', () => {
        console.log(`User disconnected with name: ${socket.user}`);
        socket.broadcast.emit('user_disconnected', {
            user: socket.user
        });
    });
});

server.listen(PORT);
