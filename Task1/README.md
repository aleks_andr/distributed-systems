# Task 1: Socket-based chat system

This system consists of two parts, the server and the chat interface. Users have the ability to choose the address
of the server they are connecting to (by default `localhost:3030` should be used).

## Usage

This system works using Docker containers orchestrated by `docker-compose`.
To start the system, run:
```
docker-compose build
docker-compose up
```

This will start the server on `localhost:3030` and the front-end on `localhost:3000`.
