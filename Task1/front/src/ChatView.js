import React from 'react';
import { Container, Button, Input, Header, Feed, Divider } from 'semantic-ui-react';
import './ChatView.css';

export default class ChatView extends React.Component {
    constructor() {
        super();
        this.state = {
            messageInput: ""
        };
    }

    getMessages() {
        // console.log(`Rendering messages: ${JSON.stringify(this.props.messages)}`);
        return this.props.messages.map((msg, i) => {
            return (<Feed.Event key={i}>
                      <Feed.Content>
                        <Feed.Summary>
                          { `${this.props.username === msg.user ? "You" : msg.user} said` }
                        </Feed.Summary>
                        <Feed.Extra text>
                          { msg.content }
                        </Feed.Extra>
                      </Feed.Content>
                    </Feed.Event>
                   );
        });
    }

    render() {
        return (<Container>
                  <Header as="h2">Chat</Header>
                  <Divider fitted/>
                  <Feed
                    size="large"
                    className="messageFeed"
                    style={ this.props.active ? { } : {backgroundColor: "#FF939340"}}>
                    {this.getMessages()}
                  </Feed>
                  <Input className="messageInput" onChange={(e, data) => this.setState({ messageInput: data.value })}/>
                  <Button
                    content="Send"
                    color={this.state.loggedIn ? "red" : "green"}
                    onClick={() => this.props.sendMessage(this.state.messageInput)}/>
                </Container>);
    }
}
