import React from 'react';
import io from 'socket.io-client';
import { Container, Button, Input, Segment } from 'semantic-ui-react';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import ChatView from './ChatView.js';
import './App.css';

class App extends React.Component {

    constructor() {
        super();
        this.state = {
            loggedIn: false,
            username: "",
            messages: []
        };

        this.connectToServer = this.connectToServer.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
        this.disconnect = this.disconnect.bind(this);
    }

    connectToServer() {
        console.log(this.state.addressInput);
        console.log(this.state.nameInput);

        const username = this.state.nameInput;

        this.socket = io(this.state.addressInput, { query: { username} } );
        this.socket.on('connect', () => {
            this.setState({ loggedIn: true, username: username }, () => {
                NotificationManager.success("Successfully connected to server.", "Connection");
            });
        });

        this.socket.on('message', (data) => {
            this.setState({
                messages: [...this.state.messages, { user: data.user, content: data.message }]
            });
        });

        this.socket.on('disconnect', () => {
            this.setState({ loggedIn: false, username: "" }, () => {
                NotificationManager.warning("Disconnected from chat server.", "Connection");
            });
        });
    }

    disconnect() {
        if (this.socket) {
            this.socket.disconnect();
        }
    }

    sendMessage(message) {
        console.log(`Attempting to send message: ${message}`);
        if (this.socket) {
            console.log(`Sending message: ${message}`);
            this.socket.emit('message', message);
        }
    }

    render() {
        return(
            <Container style={{ height: '100vh' }}>
              <Segment>
                <Input
                  placeholder="Server address: (localhost:3030)"
                  onChange={(e, data) => this.setState({ addressInput: data.value })}/>
                <Input
                  placeholder="Username..."
                  onChange={(e, data) => this.setState({ nameInput: data.value })}/>
                <Button
                  color={this.state.loggedIn ? "red" : "green"}
                  content={this.state.loggedIn ? "Disconnect" : "Join chat"}
                  onClick={this.state.loggedIn ? this.disconnect : this.connectToServer}
                />
              </Segment>

              <ChatView
                active={this.state.loggedIn}
                username={this.state.username}
                messages={this.state.messages}
                sendMessage={this.sendMessage}
              />

              <NotificationContainer/>
            </Container>
        );
    }
}

export default App;
