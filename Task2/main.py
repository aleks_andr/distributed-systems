import sys
import time

import pickle
import xmltodict
import dicttoxml
import json
import yaml
import msgpack

N_OPERATIONS=10000

def main():
    
    print("Initial data is loaded from 'data.json'")
    data = time_execution(load_initial_data, "Loading initial data")
    
    run_native()
    run_xml()
    run_json()
    run_yaml()
    run_msgpack()


def load_initial_data():
    print("Loading data into dictionary...")

    with open("data.json") as f:
        data = json.load(f)
        return data


def time_execution(func, verbal_name, repeat=1):
    print("Running '{}' {} times".format(verbal_name, repeat))

    result = None
    start = time.time()

    for i in range(1, repeat):
        result = func()
    end = time.time()

    print("'{}' x{} took {:.4f} seconds".format(verbal_name, repeat, end-start))
    print("")

    return result

def run_native(data={}):
    # Serialize dict to Native (Pickle)
    with open('out.p', 'wb') as outf:
        time_execution(lambda: pickle.dump(data, outf), "Serializing data to Pickle file", repeat=N_OPERATIONS)
        outf.close()

    # Deserialize Pickle file to dict
    with open('out.p', 'rb') as inputf:
        time_execution(lambda: pickle.load(inputf), "Deserializing data from Pickle file", repeat=N_OPERATIONS)
        inputf.close()

def run_xml(data={}):
    # Serialize dict to XML
    xml = time_execution(lambda: dicttoxml.dicttoxml(data), "Serializing data to XML", repeat=N_OPERATIONS)
    # Deserialize XML into a dict
    output = time_execution(lambda: xmltodict.parse(xml), "Deserializing data from XML", repeat=N_OPERATIONS)
    
def run_json(data={}):
    # Serialize dict to JSON
    json_string = time_execution(lambda: json.dumps(data), "Serializing data to JSON", repeat=N_OPERATIONS)
    # Deserialize JSON to a dict
    output = time_execution(lambda: json.loads(json_string), "Deserializing data from JSON", repeat=N_OPERATIONS)

def run_msgpack(data={}):
    # Serialize dict to MessagePack
    binary = time_execution(lambda: msgpack.packb(data), "Serializing data to MessagePack", repeat=N_OPERATIONS) 
    # Deserialize MessagePack to a dict
    output = time_execution(lambda: msgpack.unpackb(binary), "Deserializing data from MessagePack", repeat=N_OPERATIONS)

def run_yaml(data={}):
    # Serialize dict to YAML
    yaml_string = time_execution(lambda: yaml.dump(data, Dumper=yaml.Dumper), "Serializing data to YAML", repeat=N_OPERATIONS)
    # Deserialize YAML to a dict
    output = time_execution(lambda: yaml.load(yaml_string, Loader=yaml.Loader), "Deserializing data from YAML", repeat=N_OPERATIONS)

if __name__ == '__main__':
    main()
