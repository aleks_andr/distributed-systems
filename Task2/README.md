# Task 2: Data serialization

This program is a simple Python script which measures (de-)serialization performance for various formats.
For more accurate results, the script performs the same operations a large number of times (10000 by default).
On startup, the data is loaded from a large sample JSON file.

## Usage

This program is intended to be executed inside a Docker container to make dependency management easier.

To run the script use:
```
docker build -t task2 .
docker run -it task2
```

NOTE: If the container does not produce output on subsequent runs, you may need to remove the old container.
This can be done with `docker rm <container_id>`.

